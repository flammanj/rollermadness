﻿using UnityEngine;

public class TowerBehaviour : MonoBehaviour
{
    public Transform target;
    public GameObject eye;
    public GameObject lens;
    public float range = 15f;
    public float maxCharge = 20f;
    public float turnSpeed = 20f;
    private float charge = 0f;
    private bool shooting = false;
    private LineRenderer line;
    


    
    // Start is called before the first frame update
    void Start()
    {
        line = lens.GetComponent<LineRenderer>();
        line.enabled = false;

//        Screen.lockCursor = true;
        
        eye = transform.Find("Eye").gameObject;
        if (!eye)
        {
            Debug.LogError("Eye not found");
        }

        // if no target specified, assume the player
        if (target == null) {

            if (GameObject.FindWithTag ("Player")!=null)
            {
                target = GameObject.FindWithTag ("Player").GetComponent<Transform>();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            charge = 0f;
            return;
        }
            

        //get the distance between the tower and the target
        float distance = Vector3.Distance(eye.transform.position,target.position);

        //try shooting if target is in range
        if (distance < range)
        {
            if (!shooting)
            {
                increaseCharge();
            }
            shoot();
        }
        else
        {
            decreaseCharge();
        }
    }
    
    // Set the target of the chaser
    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    void increaseCharge()
    {
        charge += 0.1f;
        if (charge > maxCharge)
        {
            charge = maxCharge;
        }
    }

    void decreaseCharge()
    {
        charge -= 0.1f;
        if (charge < 0)
        {
            charge = 0;
        }
    }

    void shoot()
    {
        if (target == null)
        {
            return;
        }
        
        if (charge >= maxCharge)
        {
            shooting = true;
        }

        if (!shooting)
        {
            return;
        }

        decreaseCharge();
        FireLaser();
        
        if (charge == 0)
        {
            line.enabled = false;
            shooting = false;
        }
    }
    
    void FireLaser()
    {
        line.enabled = true;


        Ray ray = new Ray(lens.transform.position, lens.transform.forward);
        RaycastHit hit;
            
        line.SetPosition(0, ray.origin);
        if (Physics.Raycast(ray, out hit, 100))
        {
            line.SetPosition(1, hit.point);
            if (hit.rigidbody)
            {
                
                hit.rigidbody.AddForceAtPosition(lens.transform.forward * 5, hit.point);
                if (hit.collider.tag.Equals("Player"))
                {
                    var health = target.GetComponent<Health>();
                    health.healthPoints -= 1;
                }    
            }
        }
        else
        {
            line.SetPosition(1, ray.GetPoint(100));
        }
    }
}
